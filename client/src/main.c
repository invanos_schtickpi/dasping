#include <stdio.h>
#include <netdb.h>
#include <signal.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

//global var
static struct addrinfo * addr_g = NULL;
static char const * port_g = "4999";
static char const * domain_g = "localhost";
static struct timespec begin;
static struct timespec end;

//function in main
static void set_signal();
static void parse_head(int argc, char ** argv);
static void getIP();
static void cleanIP();
static void reportIP();

//UTIL
static _Noreturn void terminate(char const *);

//SEGMENT::MAIN
int main(int argc, char ** argv)
{
  int socketID;
  struct addrinfo * temadd = NULL;
  set_signal();
  parse_head(argc, argv);
  getIP();
  reportIP();
  temadd = addr_g;
  while(temadd != NULL){
    printf("logging\n");
    socketID = socket(temadd->ai_family, temadd->ai_socktype, temadd->ai_protocol);
    if(socketID == -1){
      temadd = temadd->ai_next;
      continue;
    }
    alarm(20);
    clock_gettime(CLOCK_MONOTONIC_RAW, &begin);
    //time critical area
    if(connect(socketID, temadd->ai_addr, temadd->ai_addrlen) == -1){
      close(socketID);
      temadd = temadd->ai_next;
      continue;
    }
    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    break;
  }
  if((socketID != -1) && (temadd != NULL)){
    close(socketID);
    cleanIP();
    printf("time taken is %lu seconds, %ld nano seconds\n",
           (end.tv_sec - begin.tv_sec), (end.tv_nsec - begin.tv_nsec));
    printf("ping is %lu seconds, %ld nano seconds\n",
           ((end.tv_sec - begin.tv_sec) / 2), ((end.tv_nsec - begin.tv_nsec) / 2));
    return 0;
  }
  else{
    cleanIP();
    terminate("unable to connect");
  }
}

//SEGMENT::IP
void getIP()
{
  int stat;
  struct addrinfo hint;
  (void)memset(&hint, 0, sizeof(struct addrinfo));
  hint.ai_family = AF_UNSPEC;
  hint.ai_socktype = SOCK_STREAM;
  if((stat = getaddrinfo(domain_g, port_g, &hint, &addr_g)) != 0){
    terminate(gai_strerror(stat));
  }
}

void cleanIP()
{
  freeaddrinfo(addr_g);
}
void reportIP()
{
  char ipstring[INET6_ADDRSTRLEN];
  struct addrinfo * p = NULL;
  for(p = addr_g; p != NULL; p = p->ai_next){
    void * address;
    if(p->ai_family == AF_INET){
      //IP4
      struct sockaddr_in *ip4 = (struct sockaddr_in *)p->ai_addr;
      address = &(ip4->sin_addr);
    }
    else{
      //IP6
      struct sockaddr_in6 *ip6 = (struct sockaddr_in6 *)p->ai_addr;
      address = &(ip6->sin6_addr);
    }
    inet_ntop(p->ai_family, address, ipstring, INET6_ADDRSTRLEN);
    printf("Possible: %s port number: %s, to %s\n",
           (p->ai_family == AF_INET ? "IPv4" : "IPv6"),  port_g, ipstring);
  }
}

//SEGMENT::UTILITIES
_Noreturn void terminate(char const * str)
{
  perror(str);
  exit(-1);
}

//SEGMENT::Parser
enum Token {
  QUESTION,
  USAGE,
  VERSION,
  STRING,
  DIGIT,
  UNKNOWN
};
static enum Token tokenize(char const *);
void parse_head(int argc, char ** argv)
{
  for(int i = 1; i < argc; i++){
    switch(tokenize(argv[i])){
    case QUESTION:
      puts("Client of pinger. made by: X33");
      goto US;
      break;
    case USAGE:
    US:
      printf("USAGE: %s [-v | --version] [-? | -h | --help] [-u | --usage] DOMAIN PORT\n", argv[0]);
      exit(0);
    case VERSION:
      puts("unsupported");
      exit(0);
    case DIGIT:
      port_g = argv[i];
      break;
    case STRING:
      domain_g = argv[i];
      break;
    default:
      terminate("unknown token");
    }
    printf("using domain: %s and port %s\n", domain_g, port_g);
  }
}

static int is_digit(char const *);
enum Token tokenize(char const * str)
{
  if(str[0] == '-'){
    if((str[1] == 'u') || !strcmp((str + 1), "-usage")){
      return USAGE;
    }
    if((str[1] == '?') || (str[1] == 'h') ||strcmp((str + 1), "-help")){
      return QUESTION;
    }
    if((str[1] == 'v') ||strcmp((str + 1), "-version")){
      return VERSION;
    }
    return UNKNOWN;
  }
  else{
    return (is_digit(str) ? DIGIT : STRING);
  }
}

int is_digit(char const * str)
{
  int len = strlen(str);
  for(int i  = 0; i < len; i++){
    switch(str[i]){
    default:
      return 0;
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
      break;
    }
  }
  return len;
}

//SEGMENT::SIGNAL
static _Noreturn void allgemeint(int);
static _Noreturn void alrm(int);
void set_signal()
{
  struct sigaction sa;
  sa.sa_handler = alrm;
  (void)sigemptyset(&(sa.sa_mask));
  sa.sa_flags = 0;
  if (sigaction(SIGALRM, &sa, NULL) == -1){
    terminate("Signaling");
  }
  sa.sa_handler = allgemeint;
  if (sigaction(SIGINT, &sa, NULL) == -1){
    terminate("Signaling");
  }
}

void alrm(int i)
{
  (void)i;
  errno = EREMOTEIO;
  terminate("Hosts not responding for more than 20 seconds");
}
void allgemeint(int i)
{
  (void)i;
  terminate("Interrupted");
}
