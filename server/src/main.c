#include <stdio.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>

static char const * g_portnum = "4999";
static int open_socket(void);
static void set_sig(void);
static void parse_head(int argc, char ** argv);
static _Noreturn void terminate(char const *);

int main(int argc, char ** argv)
{
  int list_socket;
  parse_head(argc, argv);
  set_sig();
  if((list_socket = open_socket()) == -1){
    terminate("Unable to open Socket");
  }
  close(accept(list_socket, NULL, NULL));
  close(list_socket);
  return 0;
}

//SEGMENT::UTILITIES
_Noreturn void terminate(char const * chr)
{
  fputs(chr, stderr);
  fputc('\n', stderr);
  exit(-1);
}


//SEGMENT::SOCKET
static int backqueue = 10;
static int set_bind(struct addrinfo * i, int * ret);
int open_socket(void)
{
  struct addrinfo hint;
  struct addrinfo * addrlink = 0;
  int adretcode;
  int sockcode;
  memset(&hint, 0, sizeof(struct addrinfo));
  hint.ai_family = AF_UNSPEC;
  hint.ai_socktype = SOCK_STREAM;
  hint.ai_flags = AI_PASSIVE;
  adretcode = getaddrinfo(NULL, g_portnum, &hint, &addrlink);
  if(adretcode != 0){
    return -1;
  }
  if(set_bind(addrlink, &sockcode) == -1){
    return -1;
  }
  freeaddrinfo(addrlink);
  if(listen(sockcode, backqueue) == -1){
    int errn = errno;
    close(sockcode);
    errno = errn;
    return -1;
  }
  return sockcode;
}

int set_bind(struct addrinfo * i, int * ret)
{
  int yes = 1;
  int errn;
  for(; i != NULL; i=i->ai_next){
    *ret = socket(i->ai_family, i->ai_socktype, i->ai_protocol);
    if(*ret == -1){
      continue;
    }
    if(setsockopt(*ret, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1){
      return -1;
    }
    if(bind(*ret, i->ai_addr, i->ai_addrlen) == -1){
      errn = errno;
      close(*ret);
      continue;
    }
    break;
  }
  if(i == NULL){
    errno = errn;
    return -1;
  }
  return 0;
}


//SEGMENT::SIGNALING
static void handlesig(int);
void set_sig(void)
{
  struct sigaction sa;
  sa.sa_handler = handlesig;
  (void)sigemptyset(&(sa.sa_mask));
  sa.sa_flags = 0;
  if (sigaction(SIGINT, &sa, NULL) == -1){
    terminate("Signaling");
  }
}
void handlesig(int i)
{
  (void)i;
}


//SEGMENT::COMMAND LINE PARSER
static int digit_only(char const *);

void parse_head(int argc, char ** argv)
{
  char const * newer = g_portnum;
  for(int i = 1; i < argc; ++i){
    if(digit_only(argv[i])){
      newer = argv[i];
    }
  }
  if(newer == g_portnum){
    printf("No valid arg detected. Using %s as port number\n", g_portnum);
    fflush(stdout);
  }
  else{
    g_portnum = newer;
    printf("Using %s as port number\n", g_portnum);
    fflush(stdout);
  }
}

int digit_only(char const * in)
{
  size_t lng = strlen(in);
  for(size_t i = 0; i < lng; ++i){
    switch(in[i]){
    default:
      return 0;
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
      break;
    }
  }
  return lng;
}
